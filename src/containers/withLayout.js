import React from "react";
import { AppContext } from "../context/index";
import Sidebar from "./sidebar";
const withLayout = (Component) => (props) => {
  const { contextState, setContextState } = React.useContext(AppContext);
  console.log(AppContext);
  return (
    <div className="container-fluid">
      <div className="row wrap">
        <Sidebar
          contextState={contextState}
          setContextState={setContextState}
        />
        <div className="main-wrap">
          <Component {...props} />
        </div>
      </div>
    </div>
  );
};

export default withLayout;
