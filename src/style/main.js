import scssvar from "../style/components/_variables.scss";

export const fzBig = scssvar.fzBig;
export const fzMedium = scssvar.fzMedium;
export const fzBasic = scssvar.fzBasic;
export const fzSmall = scssvar.fzSmall;

export const gray = scssvar.gray;
export const tiffanyGradient_A = scssvar.tiffanyGradient_A;
export const tiffanyGradient_B = scssvar.tiffanyGradient_B;
export const mainSectionBg = scssvar.mainSectionBg;

export const backgroundGradient = {
  background: `linear-gradient(to left, ${scssvar.tiffanyGradient_A}, ${scssvar.tiffanyGradient_B})`,
};
export const borderGradient = {
  width: 120,
  height: 40,
  border: "double 2px transparent",
  borderRadius: "25px",
  backgroundOrigin: "border-box",
  backgroundClip: "content-box, border-box",
  backgroundImage: `linear-gradient(black, black),
                      radial-gradient(circle at top left,
                    ${scssvar.tiffanyGradient_A},${scssvar.tiffanyGradient_B})`,
};
