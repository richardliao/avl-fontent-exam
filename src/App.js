import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from "./pages/Home";
import "bootstrap/dist/css/bootstrap.min.css";
import "./style/main.scss";

function App() {
  return (
      <div className="App">
        <Switch>
          <Route path="/home">
            <Home />
          </Route>
        </Switch>
      </div>
  );
}

export default App;
