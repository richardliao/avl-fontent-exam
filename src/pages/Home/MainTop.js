import React from "react";
import CheckboxTreeMenu from "../../components/useCheckboxTreeMenu";
import MoreFilter from "../../components/useMoreFilter";
import Select from "../../components/material/select";

import useMediaQuery from "@material-ui/core/useMediaQuery";
const MainTop = () => {
  const matchesXS = useMediaQuery("(max-width:767px)");
  return (
    <div>
      {/* <div className="mr-4" style={{ position: "relative", top: 14 }}>
        <MoreFilter />
      </div> */}

      <div className="d-flex">
        <div className="mr-4" style={{ flex: matchesXS ? 1 : "initial" }}>
          <Select
            placeholder="地區"
            options={[
              { id: 0, value: "0", label: "all" },
              { id: 1, value: "1", label: "fruits" },
            ]}
            getSelected={(val) => {
              console.log(val);
            }}
          />
        </div>
        <div className="mr-4 " style={{ flex: matchesXS ? 1 : "initial" }}>
          <Select
            placeholder="地區"
            options={[
              { id: 0, value: "0", label: "all" },
              { id: 1, value: "1", label: "fruits" },
            ]}
            getSelected={(val) => {
              console.log(val);
            }}
          />
        </div>
        <div
          className="mr-sm-4"
          style={{
            flex: matchesXS ? 1 : "initial",
            position: "relative",
            top: 14,
          }}
        >
          <CheckboxTreeMenu />
        </div>

        {matchesXS ? null : (
          <div className="mr-4" style={{ position: "relative", top: 14 }}>
            <MoreFilter />
          </div>
        )}
      </div>
    </div>
  );
};

export default MainTop;
