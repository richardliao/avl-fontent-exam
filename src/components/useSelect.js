import React from "react";
import Select from "./material/select";
const GroupSelect = () => {
  return (
    <Select
      placeholder="123"
      options={[
        {
          id: 1,
          label: "A",
          value: "A",
        },
      ]}
      getSelected={(v) => {
        console.log(v);
      }}
    />
  );
};

export default GroupSelect;
