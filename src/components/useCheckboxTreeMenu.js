import React from "react";
// import { makeStyles } from "@material-ui/core/styles";
import Menu from "./material/menu";
import Treewithcheckbox from "./treewithcheckbox/index";

// const useStyles = makeStyles((theme) => ({
//   nested: {
//     paddingLeft: 0,
//   },
//   ListSubheader: {
//     color: "#fff",
//   },
// }));

const MoreFilter = () => {
  const [menuTitle, setMenuTitle] = React.useState("tree checkbox");
  const [selectedTree, setSelectedTree] = React.useState([]);

  React.useEffect(() => {
    const arr = [];
    selectedTree.map((v, i) => {
      if (v.checked) {
        arr.push(v.label);
      }
      return false
    });
    if (arr.length === 0) {
      setMenuTitle("tree checkbox");
    } else {
      setMenuTitle(arr.toString());
    }
  }, [selectedTree]);
  return (
    <Menu title={menuTitle}>
      <Treewithcheckbox
        tree={[
          {
            id: 1,
            label: "Arithmetic-A",
            checked: false,
            children: [
              {
                id: 2,
                label: "Subtopic",
                checked: false,
              },
              {
                id: 3,
                label: "Subtopic",
                checked: false,
              },
              {
                id: 4,
                label: "Subtopic",
                checked: false,
              },
            ],
          },
          {
            id: 5,
            label: "Arithmetic-B",
            checked: false,
          },
        ]}
        gteOrgStructure={(data) => {
          setSelectedTree(data);
        }}
      />
    </Menu>
  );
};

export default MoreFilter;
