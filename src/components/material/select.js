import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import ListSubheader from "@material-ui/core/ListSubheader";
import Select from "@material-ui/core/Select";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import FormControl from "@material-ui/core/FormControl";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import clsx from "clsx";
import CloseSharpIcon from "@material-ui/icons/CloseSharp";
import { backgroundGradient, mainSectionBg, fzBasic } from "../../style/main";
const borderGradient = {
  border: "2px solid",
  borderImageSlice: 1,
  borderWidth: "2px",
  borderImageSource: "linear-gradient(to left, #00bc9b, #5eaefd)",
};
const useStyles = makeStyles((theme) => ({
  select: {
    ...borderGradient,
    color: "#fff",
    borderRadius: "25px",
    textAlign: "center",
    paddingRight: 0,
  },
  label: {
    fontSize: fzBasic,
    color: "#fff",
    borderRadius: "25px",
    left: 10,
    transformOrigin: "top left",
    // transform: "translate(-50%, 24px) scale(1)",
    "&.MuiInputLabel-shrink": {
      left: 0,
      // transform: "translate(0, 1.5px) scale(0.75)",
    },
    "&.Mui-focused": {
      left: 0,
      // transform: "translate(0, 1.5px) scale(0.75)",
      color: "#fff",
    },
  },
  formControl: {
    width: "100%",
  },
  icon: {
    fill: "#fff",
  },
  menuPaper: {
    backgroundColor: "black",
    color: "#fff",
  },
  menuPaperList: {
    "& .MuiListItem-root": {
      "&:hover": {
        ...backgroundGradient,
      },
      "&.Mui-selected": {
        ...backgroundGradient,
      },
    },
  },
  ListSubheader: {
    color: "#fff",
  },
  SwipeableDrawer: {
    background: mainSectionBg,
  },
}));

export default function GroupedSelect({ placeholder, options, getSelected }) {
  const classes = useStyles();
  const matchesXS = useMediaQuery("(max-width:767px)");
  const [age, setAge] = React.useState("");
  const [open, setOpen] = React.useState(false);
  const [state, setState] = React.useState({
    bottom: false,
  });
  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const handleChange = (e, value) => {
    setAge(value);
    getSelected(value);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    if (matchesXS) {
      setState({ ...state, bottom: true });
      return false;
    }
    setState({ ...state, bottom: false });
    setOpen(true);
  };

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === "top" || anchor === "bottom",
      })}
      role="presentation"
      // onClick={toggleDrawer(anchor, false)}
      // onKeyDown={toggleDrawer(anchor, false)}
      style={{
        position: "relative",
        paddingTop: 30,
        paddingBottom: 30,
      }}
    >
      <button
        style={{
          position: "absolute",
          top: 10,
          right: 30,
          border: "none",
          backgroundColor: "transparent",
          color: "#fff",
        }}
        onClick={toggleDrawer(anchor, false)}
      >
        <CloseSharpIcon />
      </button>
      <ListSubheader
        classes={{ root: classes.ListSubheader }}
        style={{ paddingLeft: 60 }}
        disabled
      >
        Topics
      </ListSubheader>
      <List>
        {options &&
          options.map((option) => (
            <ListItem
              button
              key={option.label}
              onClick={(e) => {
                setState({ ...state, bottom: false });
                handleChange(e, option.value);
              }}
              style={{
                color: "#fff",
                paddingLeft: 60,
                background: age === option.value ? backgroundGradient.background : null,
              }}
            >
              {option.label}
            </ListItem>
          ))}
      </List>
    </div>
  );

  return (
    <FormControl className={classes.formControl}>
      <InputLabel
        color="primary"
        htmlFor="grouped-native-select"
        classes={{
          root: classes.label,
        }}
      >
        {placeholder}
      </InputLabel>
      <Select
        id="grouped-native-select"
        defaultValue=""
        classes={{
          select: classes.select,
          icon: classes.icon,
        }}
        style={{ width: matchesXS ? "100%" : 120 }}
        value={age}
        open={open}
        onClose={handleClose}
        onOpen={handleOpen}
        onChange={(e) => handleChange(e, e.target.value)}
        MenuProps={{
          anchorOrigin: {
            vertical: "bottom",
            horizontal: "left",
          },
          transformOrigin: {
            vertical: "top",
            horizontal: "left",
          },
          getContentAnchorEl: null,
          classes: {
            paper: classes.menuPaper,
            list: classes.menuPaperList,
          },
        }}
      >
        <ListSubheader classes={{ root: classes.ListSubheader }} disabled>
          Topics
        </ListSubheader>
        {options &&
          options.map((option) => (
            <MenuItem value={option.value}>{option.label}</MenuItem>
          ))}
      </Select>
      <SwipeableDrawer
        anchor={"bottom"}
        open={state["bottom"]}
        onClose={toggleDrawer("bottom", false)}
        onOpen={toggleDrawer("bottom", true)}
        classes={{ paper: classes.SwipeableDrawer }}
      >
        {list("bottom")}
      </SwipeableDrawer>
    </FormControl>
  );
}
